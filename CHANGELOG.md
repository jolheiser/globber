## [0.1.0](https://gitea.com/jolheiser/globber/pulls?q=&type=all&state=closed&milestone=1230) - 2020-03-31

* ENHANCEMENTS
  * Move to urfave CLI (#12)
* BUILD
  * Update Makefile to match Gitea projects (#9)
  * Move tests to _test (#8)
  * Add cross-compile (#7)
  * Add CI and linter (#5)
