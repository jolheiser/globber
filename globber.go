package globber

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/gobwas/glob"
)

// Globber defines a Pattern and resulting Glob
type Globber struct {
	Pattern string
	Glob    glob.Glob
}

// GlobSet defines a list of Globbers
type GlobSet struct {
	Includes []Globber
	Excludes []Globber
}

// New returns a blank GlobSet
func New() *GlobSet {
	return &GlobSet{
		Includes: make([]Globber, 0),
		Excludes: make([]Globber, 0),
	}
}

// Match checks whether a value matches a GlobSet
func (gs *GlobSet) Match(value string) (Globber, bool) {
	var globber Globber
	var matches bool

	for _, g := range gs.Includes {
		if g.Glob.Match(value) {
			globber = g
			matches = true
			break
		}
	}

	if matches {
		for _, g := range gs.Excludes {
			if g.Glob.Match(value) {
				return globber, false
			}
		}
	}

	return globber, matches
}

// Explain returns a list of matching include and exclude patterns
func (gs *GlobSet) Explain(value string) ([]Globber, []Globber) {
	includes := make([]Globber, 0)
	excludes := make([]Globber, 0)

	for _, g := range gs.Includes {
		if g.Glob.Match(value) {
			includes = append(includes, g)
		}
	}

	for _, g := range gs.Excludes {
		if g.Glob.Match(value) {
			excludes = append(excludes, g)
		}
	}

	return includes, excludes
}

// Include appends an include Globber to a GlobSet
func (gs *GlobSet) Include(globber Globber) {
	gs.Includes = append(gs.Includes, globber)
}

// Exclude appends an exclude Globber to a GlobSet
func (gs *GlobSet) Exclude(globber Globber) {
	gs.Excludes = append(gs.Excludes, globber)
}

// Merge combines GlobSets
func (gs *GlobSet) Merge(set ...*GlobSet) {
	for _, s := range set {
		gs.Includes = append(gs.Includes, s.Includes...)
		gs.Excludes = append(gs.Excludes, s.Excludes...)
	}
}

// Search searches a base path for any files that match this GlobSet
// Returns total matches, a map of pattern -> matches, and any encountered error
func (gs *GlobSet) Search(basePath string) (int, map[string][]string, error) {
	if apn, err := filepath.Abs(basePath); err == nil {
		basePath = apn + "/"
	}

	matches := make(map[string][]string)
	total := 0
	checkPathSlash := filepath.ToSlash(basePath)
	if err := filepath.Walk(checkPathSlash, func(path string, info os.FileInfo, walkErr error) error {
		if walkErr != nil {
			return walkErr
		}

		if info.IsDir() {
			return nil
		}

		base := strings.TrimPrefix(filepath.ToSlash(path), checkPathSlash)
		g, ok := gs.Match(base)
		if ok {
			if matches[g.Pattern] == nil {
				matches[g.Pattern] = make([]string, 0)
			}
			matches[g.Pattern] = append(matches[g.Pattern], base)
			total++
		}

		return nil
	}); err != nil {
		return 0, nil, err
	}

	return total, matches, nil
}

// ParseFile parses a .globber file and returns a GlobSet
func ParseFile(filePath string, separators ...rune) (*GlobSet, error) {
	if _, err := os.Stat(filePath); err != nil {
		return nil, err
	}

	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	return ParseString(string(content), separators...)
}

// ParseString parses string data and returns a GlobSet
func ParseString(content string, separators ...rune) (*GlobSet, error) {
	set := New()
	for _, line := range strings.Split(content, "\n") {
		line = strings.TrimSpace(line)
		if line == "" || strings.HasPrefix(line, "#") {
			continue
		}

		var exclude bool
		if strings.HasPrefix(line, "!") {
			exclude = true
			line = line[1:]
		}

		g, err := glob.Compile(line, separators...)
		if err != nil {
			return nil, fmt.Errorf("'%s' is not a valid globbing pattern", line)
		}

		if exclude {
			set.Exclude(Globber{
				Pattern: line,
				Glob:    g,
			})
		} else {
			set.Include(Globber{
				Pattern: line,
				Glob:    g,
			})
		}
	}
	return set, nil
}
