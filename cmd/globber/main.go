package main

import (
	"fmt"
	"log"
	"os"

	"gitea.com/jolheiser/globber"

	"github.com/gobwas/glob"
	"github.com/urfave/cli/v2"
)

var (
	Version = "development"

	flags = []cli.Flag{
		&cli.StringFlag{
			Name:    "separator",
			Aliases: []string{"s"},
			Usage:   "Glob separator",
			Value:   "/",
		},
		&cli.StringSliceFlag{
			Name:    "include",
			Aliases: []string{"i"},
			Usage:   "Glob pattern to include",
		},
		&cli.StringSliceFlag{
			Name:    "exclude",
			Aliases: []string{"e"},
			Usage:   "Glob pattern to exclude",
		},
		&cli.StringFlag{
			Name:    "glob-file",
			Aliases: []string{"g"},
			Usage:   "Path to a glob file",
			Value:   ".globber",
		},
		&cli.StringFlag{
			Name:    "base",
			Aliases: []string{"b"},
			Usage:   "Base path to check for globs",
		},
		&cli.StringFlag{
			Name:    "explain",
			Aliases: []string{"x"},
			Usage:   "Explain why a value is include or excluded",
			Value:   "",
		},
	}
)

func main() {

	app := &cli.App{
		Name:    "globber",
		Usage:   "A CLI tool to test globs",
		Version: Version,
		Flags:   flags,
		Action:  mainAction,
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func mainAction(ctx *cli.Context) error {
	set := globber.New()
	includesFlag := ctx.StringSlice("include")
	excludesFlag := ctx.StringSlice("exclude")
	separatorsString := ctx.String("separators")
	separatorsFlag := []rune(separatorsString)

	globFileFlag := ctx.String("glob-file")

	// Load includes/excludes into set
	if err := loadSet(set, includesFlag, excludesFlag, separatorsFlag); err != nil {
		return err
	}

	// Only parse .globber file if no include pattern flags were passed
	if len(includesFlag) == 0 {
		f, err := globber.ParseFile(globFileFlag, separatorsFlag...)
		if err != nil {
			return fmt.Errorf("could not parse .globber file: %v", err)
		}
		set.Merge(f)
	}

	explainFlag := ctx.String("explain")
	if len(explainFlag) > 0 {
		return explainGlobs(set, explainFlag)
	}

	basePathFlag := ctx.String("base")
	total, matches, err := set.Search(basePathFlag)
	if err != nil {
		return fmt.Errorf("could not complete search: %v", err)
	}

	if total == 0 {
		fmt.Println("No matches found.")
		return nil
	}

	fmt.Printf("Found %d matches\n", total)
	for pattern, match := range matches {
		fmt.Printf("%s (%d)\n", pattern, len(match))
		for _, m := range match {
			fmt.Printf("\t%s\n", m)
		}
	}

	return nil
}

func loadSet(set *globber.GlobSet, includes, excludes []string, separators []rune) error {
	for _, pattern := range includes {
		g, err := glob.Compile(pattern, separators...)
		if err != nil {
			return fmt.Errorf("pattern '%s' is invalid", pattern)
		}
		set.Include(globber.Globber{
			Pattern: pattern,
			Glob:    g,
		})
	}

	for _, pattern := range excludes {
		g, err := glob.Compile(pattern, separators...)
		if err != nil {
			return fmt.Errorf("pattern '%s' is invalid", pattern)
		}
		set.Exclude(globber.Globber{
			Pattern: pattern,
			Glob:    g,
		})
	}
	return nil
}

func explainGlobs(set *globber.GlobSet, explainFlag string) error {

	includes, excludes := set.Explain(explainFlag)

	if len(includes)+len(excludes) == 0 {
		return fmt.Errorf("'%s' is not matched by any pattern(s)", explainFlag)
	}

	if len(includes) > 0 {
		fmt.Printf("'%s' is included by the following pattern(s):\n", explainFlag)
		for _, include := range includes {
			fmt.Printf("\t%s\n", include.Pattern)
		}
	}

	fmt.Println()

	if len(excludes) > 0 {
		fmt.Printf("'%s' is excluded by the following patterns:\n", explainFlag)
		for _, exclude := range excludes {
			fmt.Printf("\t%s\n", exclude.Pattern)
		}
	}
	return nil
}
