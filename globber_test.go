package globber

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestGlobber(t *testing.T) {
	set, err := ParseFile(".globber", '/')
	if err != nil {
		t.Logf("Could not parse .globber: %v", err)
		t.FailNow()
	}

	if len(set.Includes) != 4 {
		t.Logf("Expected 4 include patterns, but got %d", len(set.Includes))
		t.Fail()
	}

	if len(set.Excludes) != 2 {
		t.Logf("Expected 2 exclude patterns, but got %d", len(set.Excludes))
		t.Fail()
	}

	total, matches, err := set.Search("./_test")
	if err != nil {
		t.Logf("Could not complete search: %v", err)
		t.FailNow()
	}

	if total != 8 {
		t.Logf("Expected 8 matches, but got %d", total)
		t.Fail()
	}

	expected := map[string]int{
		"**.go":  3,
		"go*":    2,
		"**/go*": 1,
		"docs/*": 2,
	}

	for key, val := range expected {
		if match, ok := matches[key]; ok {
			if len(match) != val {
				t.Logf("Expected %d matches for '%s', but got %d", val, key, len(match))
				t.Fail()
			}
		} else {
			t.Logf("Pattern '%s' was not accounted for", key)
			t.Fail()
		}
	}
}
