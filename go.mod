module gitea.com/jolheiser/globber

go 1.13

require (
	github.com/gobwas/glob v0.2.3
	github.com/urfave/cli/v2 v2.1.1
)
