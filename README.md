# globber
A CLI tool to test [globs](https://github.com/gobwas/glob).

[![Build Status](https://drone.gitea.com/api/badges/jolheiser/globber/status.svg)](https://drone.gitea.com/jolheiser/globber)

### Download
```
go get -u gitea.com/jolheiser/globber/cmd/globber
globber -h
```

### Usage
Give globber some `patterns` and `separators` to test a directory for matching globs.  
If no separators are passed, globber defaults to using only `/`

```
globber -p "**.go" -s "/"
```

### .globber file
Rather than passing many flags, you can instead point globber to a `.globber` file, which closely resembles a `.gitignore` file.  
Then only separators need to be passed, if wanted.

[Example](.globber)